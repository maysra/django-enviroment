from django.contrib import admin
from apps.products.models import Products


@admin.register(Products)
class ProductsAdmin(admin.ModelAdmin):
    pass
