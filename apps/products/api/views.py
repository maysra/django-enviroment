from rest_framework.generics import ListCreateAPIView
from rest_framework import viewsets
from apps.products.api.serializers import ProductSerializers
from apps.products.models import Products


class ProductsModelViewSet(viewsets.ModelViewSet):
    """
    List or creating products
    """
    queryset = Products.objects.all()
    serializer_class = ProductSerializers
