from django.conf.urls import url, include
from django.contrib import admin
from rest_framework.schemas import get_schema_view
from rest_framework_swagger.renderers import SwaggerUIRenderer, OpenAPIRenderer, OpenAPICodec


schema_view = get_schema_view(title='Nabni API v.1', renderer_classes=[OpenAPIRenderer, SwaggerUIRenderer, OpenAPICodec])

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^api/v1/', include('apps.products.urls', namespace="products")),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^', schema_view, name="docs"),
]
