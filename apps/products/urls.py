from rest_framework import routers

from apps.products.api.views import ProductsModelViewSet

router = routers.DefaultRouter()

product_list = ProductsModelViewSet.as_view({'get': 'list'})
product_detail = ProductsModelViewSet.as_view({'get': 'retrieve'})

router.register(r'products', ProductsModelViewSet, base_name='products_api')
urlpatterns = router.urls
