from django.db import models


class Products(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    name = models.CharField(verbose_name="Product name", max_length=255)
    image = models.ImageField(upload_to="products")

    def __str__(self):
        return self.name

    class Meta:
        ordering = ('created_at',)
