from rest_framework import serializers
from apps.products.models import Products


class ProductSerializers(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Products
        fields = ('id', 'name', 'image', 'created_at', 'updated_at')
